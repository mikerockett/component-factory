![Made with Vue](https://forthebadge.com/images/badges/made-with-vue.svg)
![Does not contain tree nuts](https://forthebadge.com/images/badges/does-not-contain-treenuts.svg)
![Built with Love](https://forthebadge.com/images/badges/built-with-love.svg)

# Vue Component Factory

Automated registration of Vue Components, using a static or lazy factory. [Learn more](https://rockett.pw/blog/component-factories-for-vue).

```shell
npm install @rockett/component-factory
```
