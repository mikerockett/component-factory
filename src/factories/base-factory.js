export class BaseFactory {

  /**
   * Construct the base component factory.
   */
  constructor(requireContext, filenameReplacementPattern) {
    this.requireComponent = requireContext
    this.filenameReplacementPattern = filenameReplacementPattern
  }

  /**
   * Fetch a component module and return the component, its name
   * and the cleaned filename.
   */
  fetchComponent(contextFilename) {
    const component = this.requireComponent(contextFilename)
    const filename = contextFilename.replace(this.filenameReplacementPattern, '$1')
    const componentName = (component.default ? component.default.name : component.name) || filename

    return { componentName, component, filename }
  }

}
