import { BaseFactory } from './base-factory'

export class StaticFactory extends BaseFactory {

  /**
   * Construct the static component factory.
   */
  constructor(context, filenameReplacementPattern) {
    super(...arguments)
  }

  /**
   * Given a reference to Vue, a require.context instance
   * and a filename replacement pattern, register the
   * components based on the results of the context.
   */
  static register(instance, context, filenameReplacementPattern) {
    const factory = new this(context, filenameReplacementPattern)

    factory.requireComponent.keys().forEach(contextFilename => {
      const { componentName, component } = factory.fetchComponent(contextFilename)
      instance.component(componentName, component.default || component)
    })
  }

}
