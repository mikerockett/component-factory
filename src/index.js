import { factoryFromContext } from './builder'

export const ComponentFactory = (instance, options = {}) => {

  // Context preflight
  if (!options.context) throw 'ComponentFactory needs an instance of require.context.'

  // Break up the options
  const { context, filenameReplacementPattern } = options

  // Instantiate the applicable factory and register the components with the Vue instance
  // The context must be passed in explicitly as Webpack references it at build-time.
  factoryFromContext(context).register(
    instance,
    context,
    filenameReplacementPattern || /.*\/(\w+).vue$/g
  )

}
