import { StaticFactory } from './factories/static-factory'
import { LazyFactory } from './factories/lazy-factory'

export const factoryFromContext = (context) => {
  const wantsAsync = context.id.split(' ')[1] === 'lazy'
  return wantsAsync ? LazyFactory : StaticFactory
}
